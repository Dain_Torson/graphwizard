package com.dain_torson.graphwizard.fileselector.controller.workarea;


import com.dain_torson.graphwizard.fileselector.view.workarea.ListArea;
import javafx.scene.Node;

import java.io.File;

public class ListAreaController implements ViewAreaController{

    private File [] files;
    private ListArea listArea;

    public ListAreaController(File [] files) {

        this.files = files;
        listArea = new ListArea(files);
    }

    @Override
    public void setFiles(File[] files) {
        this.files = files;
        listArea.setFiles(files);
    }

    @Override
    public Node getViewArea() {
        return listArea;
    }
}

package com.dain_torson.graphwizard.fileselector.controller.events;


import javafx.event.Event;
import javafx.event.EventType;

import java.io.File;

public class FileEvent extends Event{

    public static EventType<FileEvent> FILE_SELECTED = new EventType("FILE_SELECTED");

    private File file;

    public FileEvent(EventType<? extends Event> eventType, File file) {
        super(eventType);
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

package com.dain_torson.graphwizard.fileselector.controller;


import com.dain_torson.graphwizard.fileselector.controller.events.FileEvent;
import com.dain_torson.graphwizard.fileselector.controller.workarea.GridAreaController;
import com.dain_torson.graphwizard.fileselector.controller.workarea.ListAreaController;
import com.dain_torson.graphwizard.fileselector.controller.workarea.TableAreaController;
import com.dain_torson.graphwizard.fileselector.controller.workarea.ViewAreaController;
import com.dain_torson.graphwizard.fileselector.view.FolderItem;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Pane;

import java.io.File;
import java.util.ArrayList;

public class WorkAreaController {

    private Pane workArea;
    private String filter;
    private FolderAreaController areaController;
    private FileSelector.ViewType viewType;
    private ViewAreaController viewAreaController;
    private TextField nameField;

    private File currentFolder;

    public WorkAreaController(Pane workArea, FolderAreaController areaController, TextField nameField) {

        this.workArea = workArea;
        this.areaController = areaController;
        this.nameField = nameField;
        this.areaController.getTreeView().getSelectionModel().selectedItemProperty().
                addListener(new FolderSelectionChangedListener(this.areaController.getTreeView()));
        viewType = FileSelector.ViewType.FOLDERS;
        currentFolder = this.areaController.getHomeFolder();
        updateView();
    }

    public void setViewType(FileSelector.ViewType viewType) {
        this.viewType = viewType;
        updateView();
    }

    public void setCurrentFolder(File currentFolder) {
        this.currentFolder = currentFolder;
        updateView();
    }

    private File [] applyFilter(File [] files, String filter) {

        if(filter == null) {
            return files;
        }

        ArrayList<File> list = new ArrayList<File>();
        for(File file : files) {
            if(file.isFile()) {
                String fileName = file.getName();
                int index = fileName.lastIndexOf(".");
                if(index != -1) {
                    String ext = fileName.substring(index);
                    if(ext.equals(filter)) {
                        list.add(file);
                    }
                }
            }
            else {
                list.add(file);
            }
        }

        File [] result = new File[list.size()];

        for(int fileIdx = 0; fileIdx < list.size(); ++fileIdx) {
            result[fileIdx] = list.get(fileIdx);
        }

        return result;
    }

    private void updateView() {

        if(currentFolder == null) {
            return;
        }

        File[] files = currentFolder.listFiles();
        files = applyFilter(files, filter);
        workArea.getChildren().clear();

        if(viewType == FileSelector.ViewType.FOLDERS) {
            viewAreaController = new GridAreaController(files);
        }
        else if(viewType == FileSelector.ViewType.LIST) {
            viewAreaController = new ListAreaController(files);
        }
        else {
            viewAreaController = new TableAreaController(files);
        }

        viewAreaController.getViewArea().addEventFilter(FileEvent.FILE_SELECTED, new FileSelectedHandler());
        workArea.getChildren().addAll(viewAreaController.getViewArea());
    }

    public void setFilter(String filter) {
        this.filter = filter;
        updateView();
    }

    public File getCurrentFolder() {
        return currentFolder;
    }

    private class FolderSelectionChangedListener implements ChangeListener {

        private TreeView<String> source;

        public FolderSelectionChangedListener(TreeView<String> source) {
            this.source = source;
        }

        @Override
        public void changed(ObservableValue observable, Object oldValue, Object newValue) {

            TreeItem<String> treeItem = (TreeItem<String>) newValue;

            if(treeItem == source.getRoot() || treeItem == null) {
                return;
            }

            FolderItem folderItem = (FolderItem) treeItem;
            currentFolder = folderItem.getFile();
            updateView();
        }
    }

    private class FileSelectedHandler implements EventHandler<FileEvent> {

        @Override
        public void handle(FileEvent event) {

            File target = event.getFile();

            if(target.isDirectory()) {
                currentFolder = event.getFile();
                updateView();
            }
            else {
                nameField.setText(target.getName());
            }
        }
    }
}

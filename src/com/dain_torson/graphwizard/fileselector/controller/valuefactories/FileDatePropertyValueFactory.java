package com.dain_torson.graphwizard.fileselector.controller.valuefactories;


import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.io.File;
import java.text.SimpleDateFormat;

public class FileDatePropertyValueFactory implements Callback<TableColumn.CellDataFeatures<File, String>, ObservableValue<String>> {

    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<File, String> param) {
        final File file = param.getValue();

        return new ObservableValue<String>() {
            @Override
            public void addListener(ChangeListener<? super String> listener) {

            }

            @Override
            public void removeListener(ChangeListener<? super String> listener) {

            }

            @Override
            public String getValue() {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                return dateFormat.format(file.lastModified());
            }

            @Override
            public void addListener(InvalidationListener listener) {

            }

            @Override
            public void removeListener(InvalidationListener listener) {

            }
        };
    }
}



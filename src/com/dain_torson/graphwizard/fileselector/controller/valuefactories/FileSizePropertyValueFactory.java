package com.dain_torson.graphwizard.fileselector.controller.valuefactories;


import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.io.File;

public class FileSizePropertyValueFactory implements Callback<TableColumn.CellDataFeatures<File, String>, ObservableValue<String>> {

    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<File, String> param) {
        final File file = param.getValue();

        return new ObservableValue<String>() {
            @Override
            public void addListener(ChangeListener<? super String> listener) {

            }

            @Override
            public void removeListener(ChangeListener<? super String> listener) {

            }

            @Override
            public String getValue() {
                if(file.isDirectory()) {
                    return "Folder";
                }
                else {
                    return String.valueOf(file.length());
                }
            }

            @Override
            public void addListener(InvalidationListener listener) {

            }

            @Override
            public void removeListener(InvalidationListener listener) {

            }
        };
    }
}


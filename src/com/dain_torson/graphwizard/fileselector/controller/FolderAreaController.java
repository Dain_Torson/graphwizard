package com.dain_torson.graphwizard.fileselector.controller;


import com.dain_torson.graphwizard.fileselector.view.FolderItem;
import javafx.event.EventHandler;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.File;

public class FolderAreaController {

    private Pane folderArea;
    private TreeView<String> treeView;
    private File homeFolder;

    public FolderAreaController(Pane folderArea, File homeFolder) {

        this.homeFolder = homeFolder;
        this.folderArea = folderArea;
        treeView = new TreeView<String>();
        treeView.setOnMouseClicked(new TreeDoubleClickedHandler(treeView));
        this.folderArea.getChildren().add(treeView);
        initFolderTree();
    }

    private void findFolder(FolderItem startFolder, String targetPath) {

        if(startFolder.getFile().getAbsoluteFile().equals(targetPath)) {
            return;
        }
        else {
            if(targetPath.contains(startFolder.getFile().getAbsolutePath())) {
                if(startFolder.getChildren().size() == 0) {
                    File files[] = startFolder.getFile().listFiles();

                    for (File file : files) {
                        if (file.isDirectory()) {
                            FolderItem item = new FolderItem(file);
                            findFolder(item, targetPath);
                            startFolder.getChildren().add(item);
                        }
                    }
                }
                else {
                    for(TreeItem treeItem : startFolder.getChildren()) {
                        FolderItem item = (FolderItem) treeItem;
                        findFolder(item, targetPath);
                    }
                }
                startFolder.setExpanded(true);
            }
        }
    }

    public void setFolder(File file) {

        TreeItem<String> rootItem = treeView.getRoot();
        for(TreeItem treeItem : rootItem.getChildren()) {
            FolderItem folderItem = (FolderItem) treeItem;
            findFolder(folderItem, file.getAbsolutePath());
        }
        rootItem.setExpanded(true);
    }

    private void initFolderTree() {

        File [] roots = File.listRoots();
        TreeItem<String> rootItem = new TreeItem<String>("Drives");
        treeView.setRoot(rootItem);
        rootItem.setExpanded(true);

        for(File root : roots) {
            FolderItem treeItem = new FolderItem(root);
            if(homeFolder != null) {
                findFolder(treeItem, homeFolder.getAbsolutePath());
            }
            rootItem.getChildren().add(treeItem);
        }
    }

    public TreeView<String> getTreeView() {
        return treeView;
    }

    public File getHomeFolder() {
        return homeFolder;
    }

    private class TreeDoubleClickedHandler implements EventHandler<MouseEvent> {

        private TreeView<String> source;

        public TreeDoubleClickedHandler(TreeView<String> source) {
            this.source = source;
        }

        @Override
        public void handle(MouseEvent event) {
            if(event.getClickCount() == 2) {

                TreeItem<String> treeItem = treeView.getSelectionModel().getSelectedItem();
                if(treeItem == source.getRoot() || treeItem.getChildren().size() > 0) {
                    return;
                }
                FolderItem folderItem = (FolderItem) treeItem;
                File [] files = folderItem.getFile().listFiles();

                if(files == null) {
                    return;
                }

                for(File file : files) {
                    if(file.isDirectory()) {
                        FolderItem item = new FolderItem(file);
                        folderItem.getChildren().add(item);
                        folderItem.setExpanded(true);
                    }
                }
            }
        }
    }
}

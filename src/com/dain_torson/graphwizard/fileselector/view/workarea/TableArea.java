package com.dain_torson.graphwizard.fileselector.view.workarea;


import com.dain_torson.graphwizard.fileselector.controller.events.FileEvent;
import com.dain_torson.graphwizard.fileselector.controller.valuefactories.FileDatePropertyValueFactory;
import com.dain_torson.graphwizard.fileselector.controller.valuefactories.FileExtPropertyValueFactory;
import com.dain_torson.graphwizard.fileselector.controller.valuefactories.FileNamePropertyValueFactory;
import com.dain_torson.graphwizard.fileselector.controller.valuefactories.FileSizePropertyValueFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

import java.io.File;


public class TableArea extends TableView<File> {

    private ObservableList<File> list;

    public TableArea(File[] files) {

        list = FXCollections.observableArrayList();

        TableColumn<File, String> tableColumn = new TableColumn<File, String>("Name");
        TableColumn<File, String> extColumn = new TableColumn<File, String>("Ext");
        TableColumn<File, String> sizeColumn = new TableColumn<File, String>("Size");
        TableColumn<File, String> dateColumn = new TableColumn<File, String>("Date");

        tableColumn.setCellValueFactory(new FileNamePropertyValueFactory());
        extColumn.setCellValueFactory(new FileExtPropertyValueFactory());
        sizeColumn.setCellValueFactory(new FileSizePropertyValueFactory());
        dateColumn.setCellValueFactory(new FileDatePropertyValueFactory());

        tableColumn.setPrefWidth(300);
        extColumn.setPrefWidth(50);
        sizeColumn.setPrefWidth(150);
        dateColumn.setPrefWidth(150);

        getColumns().addAll(tableColumn, extColumn, sizeColumn, dateColumn);
        initValues(files);

        setOnMouseClicked(new TableClickedHandler());
    }

    private void initValues(File[] files) {

        list.clear();
        for(File file : files) {
            if(file.isDirectory()) {
                list.add(file);
            }
        }

        for(File file : files) {
            if(file.isFile()) {
                list.add(file);
            }
        }
        setItems(list);
    }

    public void setFiles(File[] files) {
        initValues(files);
    }

    private class TableClickedHandler implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent event) {
            if(getSelectionModel().getSelectedItem() != null) {
                fireEvent(new FileEvent(FileEvent.FILE_SELECTED, getSelectionModel().getSelectedItem()));
            }
        }
    }

}

package com.dain_torson.graphwizard.fileselector.view.workarea;


import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.io.File;

public class GridElement extends VBox{

    private File file;
    private String name;
    private Label label;
    private Rectangle rectangle;

    public GridElement(File file) {
        this.file = file;
        rectangle = new Rectangle(100, 75);
        label = new Label();
        String iconPath = "assets/img/folderItemImg.png";
        if(file.isFile()) {
            iconPath = "assets/img/fileItemImg.png";
        }

        Image image = new Image(iconPath);
        rectangle.setFill(new ImagePattern(image));

        if(file.isDirectory()) {
            String path = file.getAbsolutePath();
            int index = path.lastIndexOf('\\');
            if(index != path.length() - 1) {
                name = path.substring(index + 1, path.length());
            }
        }
        else {
            name = file.getName();
        }

        setMaxWidth(100);
        label.setText(name);
        getChildren().addAll(rectangle, label);
        setAlignment(Pos.CENTER);
    }

    public File getFile() {
        return file;
    }
}
